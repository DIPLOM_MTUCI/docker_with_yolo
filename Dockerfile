# Образ содержаший darknet на данный моментсобран на ubuntu 18.. возможно стоит собирать свой единый образ
FROM daisukekobayashi/darknet:cpu

# установим руби 2.5.1
RUN apt-get update && apt-get install -y --no-install-recommends --no-install-suggests curl bzip2 build-essential libssl-dev libreadline-dev zlib1g-dev && \
    rm -rf /var/lib/apt/lists/* && \
    curl -L https://github.com/sstephenson/ruby-build/archive/v20180329.tar.gz | tar -zxvf - -C /tmp/ && \
    cd /tmp/ruby-build-* && ./install.sh && cd / && \
    ruby-build -v 2.5.1 /usr/local && rm -rfv /tmp/ruby-build-* && \
    gem install bundler --no-rdoc --no-ri

MAINTAINER Rulev Denis
# установим библиотеки нужные для работы рельсы и руби
RUN apt-get update && apt-get install -y ruby ruby-dev ruby-bundler build-essential libpq-dev nodejs tzdata
#RUN apt-get install -y libxslt-dev libxml2-dev

# В образе есть darknet, но основные файлы удалены. Скачаем их и разместим в нужную директорию
RUN git clone https://github.com/AlexeyAB/darknet.git && cd darknet \
    && ls && cp -r data .. && cp -r cfg .. \
    && cd .. && rm -rf darknet

RUN gem install bundler -v '2.2.14'